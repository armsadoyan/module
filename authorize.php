<?php
include_once('lib/config.lib.php');
include_once('lib/session.lib.php');
include_once('lib/shopify.php');
$storeurl=$_GET['shop'];
$_SESSION['shop'] = $storeurl ;
setcookie('shopUrl', $_SESSION['shop'], time() + (10 * 365 * 24 * 60 * 60), "/",'.sendpulse.com');
setcookie('token', $_SESSION['token'], time() + (10 *  24 * 60 * 60), "/",'.sendpulse.com');
$shop = $_COOKIE['shopUrl'];
try {
	$dbcon->query('select 1 from `tmp_store` LIMIT 1');
	$deletetable=$dbcon->query("DROP TABLE tmp_store") ;
	$sql = "CREATE TABLE tmp_store (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	storename VARCHAR(255) NOT NULL)";
	$dbcon->query($sql);
	$surl = "INSERT INTO tmp_store SET storename = '$storeurl'";
	$fsurl = $dbcon->query($surl);
} catch (Exception $e) {
	$sql = "CREATE TABLE tmp_store (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	storename VARCHAR(255) NOT NULL)";
	$dbcon->query($sql);
	$surl = "INSERT INTO tmp_store SET storename = '$storeurl'";
	$fsurl = $dbcon->query($surl);
}

if (isset($_GET['code']) || isset($_GET['shop']) ) {
	$shop=$_GET['shop'];
	$sql = "SELECT * FROM tbl_usersettings where store_name = '$shop' LIMIT 1";
	$result = $dbcon->query($sql);
	$row1=$result->fetch(PDO::FETCH_ASSOC);
	$charge_id=$row1['charge_id'];
	$planstatus=$row1['planstatus'];
	$shopifyClient = new ShopifyClient($_GET['shop'], "", SHOPIFY_API_KEY, SHOPIFY_SECRET);
	session_unset();
	$_SESSION['token'] = $shopifyClient->getAccessToken($_GET['code']);
	$_SESSION['charge_id']=$charge_id;
	if($_SESSION['token'] == '') {
	$sql = "SELECT * FROM tbl_usersettings where store_name = '$shop' LIMIT 1";
	$result = $dbcon->query($sql);
	$row=$result->fetch(PDO::FETCH_ASSOC);
	$_SESSION['token']=$row[1];
}
if ($_SESSION['token'] != '' && ($planstatus == 'pending' || $planstatus == ''))  {
	$_SESSION['shop'] = $_GET['shop'];
	// Insert Shop info to database.
	$token = $_SESSION['token'];
	$shop = $_SESSION['shop'];
	$sql = "SELECT * FROM tbl_usersettings where store_name = '$shop' LIMIT 1";
	$result = $dbcon->query($sql);
	$res=$result->rowCount();
	if ($res > 0) {
		$sql1 = "UPDATE `tbl_usersettings` SET `access_token` = '$token' WHERE `store_name` = '$shop'";
	}
	else {
		$sql1 = "INSERT INTO tbl_usersettings 
	     SET access_token = '$token',
	     store_name = '$shop'";
	}
	if ($dbcon->query($sql1) === TRUE) {
		$_SESSION['message'] = "New record created successfully";
	} else {
		$_SESSION['message'] = "Error: " . $sql . "<br>" . $con->error;
	}
	header("Location:".URL_TO_CHARGIFY);
	exit;
    } else {
		$_SESSION['shop'] = $_GET['shop'];
		header("Location: " . URL_TO_INDEX ."?shop=".$_SESSION['shop']);
		exit;
    }
} 
else if (isset($_POST['shop']) || isset($_GET['shop'])) {
	$deletetable=$dbcon->query("DROP TABLE tmp_store") ;
	$shop=$_GET['shop'];
	$shop = isset($_POST['shop']) ? $_POST['shop'] : $_GET['shop'];
	$shopifyClient = new ShopifyClient($shop, "", SHOPIFY_API_KEY, SHOPIFY_SECRET);
	$pageURL = 'http';
	if ($_SERVER["HTTPS"] == "on") { $pageURL .= "s"; }
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	header("Location: " . $shopifyClient->getAuthorizeUrl(SHOPIFY_SCOPE, $pageURL));
	exit;
}

include('templates/header.php');
include('templates/authorize.php');

?>
